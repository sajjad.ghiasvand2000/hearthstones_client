package gfx;

import constants.Constants;
import otherModels.configModel.CollectionStateModel;
import states.collectionState.CollectionState;
import states.playState.GamePlayer;

import java.awt.*;

public class Drawer {
    private static CollectionStateModel c = new CollectionStateModel();
    private static int x1 = c.get("initialXPosDeckButton");
    private static int x2 = c.get("initialYPosDeckButton");
    private static int x3 = c.get("widthDeckButton");
    private static int x4 = c.get("horizontalDistanceDeckButton");
    private static int x5 = (c.get("deckNumber") + 3);
    private static int x6 = c.get("verticalDistanceDeckButton");
    private static int x7 = (c.get("deckNumber") + 2);
    private static int x8 = c.get("heightDeckButton");

    public static void rightBorderCollection(Graphics2D g2D) {
        g2D.setColor(Color.BLACK);
        g2D.fillRect(x1, x2, x3 + 2 * x4, x5 * x6 + x7 * x8);
    }

    public static void Massage(String prompt, Graphics2D g2D, String name) {
        Font font = new Font("Helvetica", Font.BOLD, 60);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        g2D.setColor(Color.RED);
        g2D.setFont(font);
        int y;
        if (name.equals("player1") || name.equals("PLAYER1")) y = 60;
        else if (name.equals("player2") || name.equals("PLAYER2")) y = -60;
        else y = 0;
        g2D.drawString(prompt, (Constants.COMPUTER_WIDTH - width) / 2, (Constants.COMPUTER_HEIGHT - 60) / 2 + y);
    }

    public static void Wallet(String prompt, Graphics2D g2D) {
        Font font = new Font("Helvetica", Font.BOLD, 30);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(prompt, Constants.COMPUTER_WIDTH - width - 50, Constants.COMPUTER_HEIGHT - 30 - 50);
    }

    public static void drawString(Graphics2D g2D, String input, int fontSize, int x, int y) {
        Font font = new Font(input, Font.BOLD, fontSize);
        g2D.setColor(Color.BLACK);
        g2D.setFont(font);
        g2D.drawString(input, x, y);
    }

    public static void endMassage(Graphics2D g2D, String s){
        Font font = new Font("Helvetica", Font.BOLD, 60);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(s);
        g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(s, (Constants.COMPUTER_WIDTH - width) / 2, (Constants.COMPUTER_HEIGHT - 60) / 2);
    }

}
