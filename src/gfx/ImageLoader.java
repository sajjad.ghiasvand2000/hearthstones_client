package gfx;

import Entity.cards.Card;
import Entity.hero.Hero;
import otherModels.MainPlayerModel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

public class ImageLoader {

    private static ReentrantLock reentrantLock = new ReentrantLock();
    private static ReentrantLock reentrantLock1 = new ReentrantLock();
    private static ReentrantLock reentrantLock2 = new ReentrantLock();

    public static BufferedImage loadImage(String path) {
        try {
            return ImageIO.read(ImageLoader.class.getResource(path));
        } catch (IOException e) {
            e.getStackTrace();
        }
        return null;
    }

    public static BufferedImage[] loadDoubleImage(String path1, String path2) {
        try {
            return new BufferedImage[]{ImageIO.read(ImageLoader.class.getResource(path1)),
                    ImageIO.read(ImageLoader.class.getResource(path2))};
        } catch (IOException e) {
            e.getStackTrace();
        }
        return null;
    }

    public synchronized static void writeOnImage(BufferedImage bufferedImage, String name) {
        reentrantLock2.lock();
        ImageLoader.copyImage(bufferedImage, "res/texture/new hero picture/" + name + MainPlayerModel.getInstance().getUserId() + ".png");
        BufferedImage b = ImageLoader.loadImage("/texture/new hero picture/" + name + MainPlayerModel.getInstance().getUserId() + ".png");
        Graphics g = b.getGraphics();
        Font font = new Font("Helvetica", Font.BOLD, 100);
        FontMetrics fontMetrics = g.getFontMetrics(font);
        int width = fontMetrics.stringWidth(name);
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString(name, (b.getWidth() - width) / 2, (b.getHeight() + 50) / 2);
        g.dispose();
        try {
            ImageIO.write(b, "png", new File("res/texture/new hero picture/" + name + MainPlayerModel.getInstance().getUserId() + ".png"));
        } catch (IOException e) {
            e.getStackTrace();
        }
        reentrantLock2.unlock();
    }

    public synchronized static void writeOnMinionImage(BufferedImage bufferedImage, Card minion, String HP, String attack, String player, int i, int color, String type) {
        reentrantLock.lock();
        ImageLoader.copyImage(bufferedImage, "res/texture/" + type + " during play/" + minion.getName() + "_" + player + "_" + i + ".png");
        BufferedImage b = ImageLoader.loadImage("/texture/" + type + " during play/" + minion.getName() + "_" + player + "_" + i + ".png");
        assert b != null;
        Graphics g = b.getGraphics();
        Font font = new Font("Helvetica", Font.BOLD, 70);
        FontMetrics fontMetrics = g.getFontMetrics(font);
        int width = fontMetrics.stringWidth(HP);
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString(HP, b.getWidth() - 2*width + 11, b.getHeight() - 18);
        g.drawString(attack, 30, b.getHeight() - 18);
        g.dispose();
        try {
            ImageIO.write(b, "png", new File("res/texture/" + type + " during play/" + minion.getName() + color + "_" + player + "_" + i + ".png"));
        } catch (IOException e) {
            e.getStackTrace();
        }
        reentrantLock.unlock();
    }

    public synchronized static void writeOnHeroImage(BufferedImage bufferedImage, Hero hero, String HP, String player, int i, int color) {
        reentrantLock1.lock();
        ImageLoader.copyImage(bufferedImage, "res/texture/heroes during play/" + hero.getHeroClass().toString() + "_" + player + "_" + i + ".png");
        BufferedImage b = ImageLoader.loadImage("/texture/heroes during play/" + hero.getHeroClass().toString() + "_" + player + "_" + i + ".png");
        Graphics g = b.getGraphics();
        Font font = new Font("Helvetica", Font.BOLD, 40);
        FontMetrics fontMetrics = g.getFontMetrics(font);
        int width = fontMetrics.stringWidth(HP);
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString(HP, b.getWidth() - 2*width + 20, 280);
        g.dispose();
        try {
            ImageIO.write(b, "png", new File("res/texture/heroes during play/" + hero.getHeroClass().toString() + color + "_" + player + "_" + i + ".png"));
        } catch (IOException e) {
            e.getStackTrace();
        }
        reentrantLock1.unlock();
    }

    public static void copyImage(BufferedImage bufferedImage, String path) {
        try {
            ImageIO.write(bufferedImage, "png", new File(path));
        } catch (IOException e) {
            e.getStackTrace();
        }
    }
}
