package login;

import main.MainClient;
import java.io.IOException;

public class LoginModel {
    private String request;

    public LoginModel(String request) {
        this.request = request;
    }

    public String request() throws IOException {
        MainClient.getDataOutputStream().writeUTF(request);
        return MainClient.getDataInputStream().readUTF();
    }
}
