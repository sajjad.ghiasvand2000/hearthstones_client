package login;


import Entity.cards.Card;
import main.MainClient;
import main.Game;
import main.Launcher;
import otherModels.MainPlayerModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class LoginPanel extends JPanel implements ActionListener {
    private JLabel userNameLabel, passwordLabel, portLabel, ipLabel;
    private JTextField userNameField, passwordField, portField, ipField;
    private JButton login, register, ok, deletePlayer;
    private static String ip, port;

    public LoginPanel() {
        userNameLabel = new JLabel("Username:");
        passwordLabel = new JLabel("Password:");
        ipLabel = new JLabel("IP:");
        portLabel = new JLabel("Port:");
        userNameField = new JTextField(15);
        passwordField = new JTextField(15);
        ipField = new JTextField(10);
        portField = new JTextField(10);
        login = new JButton("Log In");
        register = new JButton("Register");
        ok = new JButton("Ok");
        deletePlayer = new JButton("delete account");
        login.addActionListener(this);
        register.addActionListener(this);
        ok.addActionListener(this);
        deletePlayer.addActionListener(this);
        createWindow();
    }

    private void createWindow() {

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        //FIRST ROW
        gc.gridy = 0;
        gc.weightx = 0.1;
        gc.weighty = 0.1;
        gc.gridx = 0;
        gc.insets = new Insets(20, 0, 0, -80);
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.LINE_END;
        add(ipLabel, gc);
        gc.gridx = 1;
        gc.insets = new Insets(20, 85, 0, 5);
        gc.anchor = GridBagConstraints.LINE_START;
        add(ipField, gc);

        gc.gridy = 0;
        gc.weightx = 0.1;
        gc.weighty = 0.1;
        gc.gridx = 2;
        gc.insets = new Insets(20, 0, 0, 60);
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.LINE_END;
        add(portLabel, gc);
        gc.gridx = 3;
        gc.insets = new Insets(20, -55, 0, 50);
        gc.anchor = GridBagConstraints.LINE_START;
        add(portField, gc);

        //SECOND ROW
        gc.gridy++;
        gc.weightx = 0.1;
        gc.weighty = 0.1;
        gc.gridx = 1;
        gc.insets = new Insets(10, 0, 40, -1);
        gc.anchor = GridBagConstraints.LINE_END;
        add(ok, gc);

        //THIRD ROW
        gc.gridy++;
        gc.weightx = 0.1;
        gc.weighty = 0.1;
        gc.gridx = 0;
        gc.insets = new Insets(0, 0, 0, -100);
        gc.fill = GridBagConstraints.NONE;
        gc.anchor = GridBagConstraints.LINE_END;
        add(userNameLabel, gc);
        gc.gridx = 1;
        gc.insets = new Insets(0, 105, 0, 5);
        gc.anchor = GridBagConstraints.LINE_START;
        add(userNameField, gc);

        //FORTH ROW
        gc.gridy++;
        gc.weightx = 1;
        gc.weighty = 0.1;
        gc.gridx = 0;
        gc.insets = new Insets(10, 0, 0, -100);
        gc.anchor = GridBagConstraints.LINE_END;
        add(passwordLabel, gc);
        gc.gridx = 1;
        gc.insets = new Insets(10, 105, 0, 5);
        gc.anchor = GridBagConstraints.LINE_START;
        add(passwordField, gc);

        //FIFTH ROW
        gc.gridy++;
        gc.weightx = 0.1;
        gc.weighty = 2;
        gc.gridx = 0;
        gc.insets = new Insets(0, 50, 0, -160);
        gc.anchor = GridBagConstraints.LINE_END;
        add(login, gc);
        gc.gridx = 1;
        gc.insets = new Insets(0, 170, 0, -30);
        gc.anchor = GridBagConstraints.LINE_START;
        add(register, gc);

        //SIXTH ROW
        gc.gridy++;
        gc.weightx = 0.1;
        gc.weighty = 2;
        gc.gridx = 1;
        gc.insets = new Insets(-50, 0, 0, 60);
        gc.anchor = GridBagConstraints.LINE_END;
        add(deletePlayer, gc);

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String username = userNameField.getText();
        String password = passwordField.getText();
        ip = ipField.getText();
        port = portField.getText();
        JButton button = (JButton) actionEvent.getSource();
        if (button == ok) {
            if (port.equals("") || ip.equals(""))
                LoginFrame.getTextArea().append("Enter IP or Port.\n");
            else {
                try {
                    MainClient.getInstance(ip, Integer.parseInt(port));
                } catch (NumberFormatException e) {
                    LoginFrame.getTextArea().append("The port inputs are illegal.\n");
                }
            }
        } else if (button == register) {
            if (username.equals("") || password.equals("")) {
                LoginFrame.getTextArea().append("Enter username or password.\n");
            } else {
                try {
                    String response = new LoginModel("register_" + username + "_" + password).request();
                    if (response.equals("fail")) {
                        LoginFrame.getTextArea().append("This username has already existed please choose another one!\n");
                    } else if (response.equals("pass")) {
                        LoginFrame.getTextArea().append("Congratulation!You created an account successfully.\n");
                        Launcher.setGame(new Game(1700, 1000, "Hearthstone"));
                        Launcher.getGame().start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (button == login) {
            if (username.equals("") || password.equals(""))
                LoginFrame.getTextArea().append("Enter username or password.\n");
            else {
                try {
                    String response = new LoginModel("login_" + username + "_" + password).request();
                    if (response.equals("fail")) {
                        LoginFrame.getTextArea().append("You Have Entered Wrong Username Or Password.\nPlease Try Again.\n");
                    } else if (response.equals("pass")) {
                        LoginFrame.getTextArea().append("Congratulation!Your username and password were true.\n");
                        Launcher.setGame(new Game(1700, 1000, "Hearthstone"));
                        Launcher.getGame().start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (button == deletePlayer){
            if (username.equals("")){
                LoginFrame.getTextArea().append("Enter username.\n");
            } else {
                try {
                    String response = new LoginModel("delete_" + username).request();
                    if (response.equals("fail")) {
                        LoginFrame.getTextArea().append("You Have Entered Wrong Username\n");
                    } else if (response.equals("pass")) {
                        LoginFrame.getTextArea().append("Your account deleted successfully.\n");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getIp() {
        return ip;
    }

    public static String getPort() {
        return port;
    }
}
