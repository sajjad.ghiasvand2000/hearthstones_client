package states.rankState;

import constants.Constants;
import gfx.Asserts;
import main.Handler;
import main.MainClient;
import main.Util;
import otherModels.OtherModels;
import otherModels.configModel.MenuStateModel;
import otherModels.configModel.PassiveStateModel;
import states.State;
import ui.UIManager;
import ui.UIRecImage;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class RankState extends State {
    private static MenuStateModel m = new MenuStateModel();
    private static PassiveStateModel p = new PassiveStateModel();
    private UIManager uiManager;
    private String[] topTen;
    private String[] me;
    private int x1 = p.get("initialHeightStatus");
    private int x2 = p.get("horizontalDistance");

    public RankState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);
        OtherModels.rankState();
        try {
            topTen = (String[]) MainClient.getObjectInputStream().readObject();
            me = (String[]) MainClient.getObjectInputStream().readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        menu();
        Util.exit(uiManager);
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.statusBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        int height = x1;
        for (int i = 0; i < Math.min(topTen.length, 10); i++) {
            String[] s = topTen[i].split("_");
            drawer(g2D, (i + 1) + ". " + s[0] + " --> " + s[1], height, 300);
            height += x2;
        }
        height = x1;
        for (int i = 0; i < me.length; i++) {
            if (me[i] != null) {
                String[] s = me[i].split("_");
                drawer(g2D, (s[0]) + ". " + s[1] + " --> " + s[2], height, 1000);
                height += x2;
            }
        }
        uiManager.render(g2D);
    }

    private void drawer(Graphics2D g2D, String prompt, int height, int w) {
        Font font = new Font("Helvetica", Font.BOLD, 20);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        g2D.setColor(Color.CYAN);
        g2D.setFont(font);
        g2D.drawString(prompt, w, height);
    }

    private void menu() {
        uiManager.addButton(new UIRecImage(m.get("initialXPosMenu"), m.get("initialYPosMenu"), m.get("widthMenu"), m.get("heightMenu"),
                Asserts.menu, () -> {
            State.setCurrentState(handler.getMenuState());
            handler.getCollectionState().updateCardInit();
            OtherModels.body("click menu button", "back to menu sate");
        }));
    }
}
