package states.changeCardState;

import Entity.Deck;
import Entity.cards.Card;
import constants.Constants;
import gfx.Asserts;
import gfx.ImageLoader;
import login.LoginPanel;
import main.Handler;
import main.MainClient;
import otherModels.OtherModels;
import otherModels.configModel.PassiveStateModel;
import states.State;
import states.collectionState.CardInitUtils;
import states.playState.PlayState;
import ui.UICardImage;
import ui.UIManager;
import ui.UIRecImage;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChangeCardState extends State {

    private PassiveStateModel p = new PassiveStateModel();
    private Deck deck;
    private UIManager uiManager;
    private ArrayList<UICardImage> uiCardImages;
    private boolean[] change;

    public ChangeCardState(Handler handler) {
        super(handler);
//        if (!Utils.isChangeCardNumber()){
//            deck = handler.getCollectionState().getPlayDeck("player1");
//        }else {
//            deck = handler.getCollectionState().getPlayDeck("player2");
//        }
        deck = OtherModels.getDeck();
        change = new boolean[3];
        if (!handler.getCollectionState().getConstantButton().isDeckReader())
            Collections.shuffle(deck.getCards());
        uiManager = new UIManager(handler);
        uiCardImages = new ArrayList<>();
        cards();
        uiManager.setButtons(CardInitUtils.toUIObject(uiCardImages));
        done();
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.passiveBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        uiManager.render(g2D);
    }

    private void cards() {
        int x = p.get("initialXPosCard");
        int y = p.get("initialYPosCard");
        for (int i = 0; i < 3; i++) {
            Card card = deck.getCards().get(i);
            int finalI = i;
            uiCardImages.add(new UICardImage(card, x, y, p.get("widthCard"), p.get("heightCard"),
                    ImageLoader.loadDoubleImage(card.getTexturePath()[0], card.getTexturePath()[7]), () -> {
                change[finalI] = true;
            }));
            x += p.get("widthCard") + p.get("horizontalDistanceCard");
        }
    }

    private void done() {
        uiManager.addButton(new UIRecImage(p.get("changeCardinitialXPos"), p.get("changeCardinitialYPos"), p.get("changeCardDoneWidth"),
                p.get("changeCardDoneOkHeight"), Asserts.doneWriteHeroName, () -> {
            changeCard();
            checkNextState();
        }));
    }

    private void checkNextState() {
        start();
    }


    private void start() {
        if (!handler.getCollectionState().getConstantButton().isTraining()) {
            String s = "";
            try {
                MainClient.getDataOutputStream().writeUTF("ready");
                s = MainClient.getDataInputStream().readUTF();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (s.equals("play started")) {
                PlayState playState = new PlayState(handler);
                handler.setPlayState(playState);
                State.setCurrentState(playState);
            }
        }else {
            PlayState playState = new PlayState(handler);
            handler.setPlayState(playState);
            State.setCurrentState(playState);
        }
    }

    private void changeCard() {
        List<Card> cards = new ArrayList<>(deck.getCards());
        Card help;
        for (int i = 0; i < 3; i++) {
            if (change[i]) {
                help = cards.get(i);
                cards.set(i, cards.get(i + 3));
                cards.set(i + 3, help);
            }
        }
        deck.setCards(cards);
    }
}
