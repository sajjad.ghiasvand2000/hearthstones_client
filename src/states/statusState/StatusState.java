package states.statusState;

import constants.Constants;
import gfx.Asserts;
import main.Handler;
import main.MainClient;
import main.Util;
import otherModels.OtherModels;
import otherModels.configModel.MenuStateModel;
import otherModels.configModel.PassiveStateModel;
import states.State;
import ui.UIManager;
import ui.UIRecImage;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class StatusState extends State {
    private static MenuStateModel  m = new MenuStateModel();
    private static PassiveStateModel p = new PassiveStateModel();
    private UIManager uiManager;
    private ArrayList<String> decks;
    private int x1 = p.get("initialHeightStatus");
    private int x2 = p.get("horizontalDistance");

    public StatusState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);
        OtherModels.statusState();
        try {
            decks = (ArrayList<String>) MainClient.getObjectInputStream().readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        menu();
        Util.exit(uiManager);
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.statusBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        int height = x1;
        for (int i = 0 ; i < Math.min(decks.size(), 10) ; i++) {
            drawer(g2D, decks.get(i), height);
            height += x2;
        }
        uiManager.render(g2D);
    }

    private void drawer(Graphics2D g2D, String prompt, int height) {
        Font font = new Font("Helvetica", Font.BOLD, 20);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        g2D.setColor(Color.CYAN);
        g2D.setFont(font);
        g2D.drawString(prompt, (Constants.COMPUTER_WIDTH - width) / 2, height);
    }

    private void menu() {
        uiManager.addButton(new UIRecImage(m.get("initialXPosMenu"), m.get("initialYPosMenu"), m.get("widthMenu"), m.get("heightMenu"),
                Asserts.menu, () -> {
            State.setCurrentState(handler.getMenuState());
            handler.getCollectionState().updateCardInit();
            OtherModels.body("click menu button", "back to menu sate");
        }));
    }
}