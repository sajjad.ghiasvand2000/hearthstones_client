package states.playState;

import Entity.cards.Card;
import Entity.cards.minion.Minion;
import Entity.cards.weapon.Weapon;
import Entity.hero.Hero;
import constants.Constants;
import gfx.ImageLoader;
import otherModels.configModel.PlayStateModel;
import ui.UIDoubleImage;

import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class GraphicUtils {
    private static PlayStateModel p = new PlayStateModel();
    private static int x1 = p.get("initialXPosDevelopmentPercent");
    private static int x3 = p.get("initialXPosTimer");
    private static int x4 = p.get("initialYPosTimer");
    private static int x5 = p.get("initialXPosNumberCardInDeck");
    private static int x7 = p.get("initialXPosMana");
    private static int x8 = p.get("maxWidthDeck");
    private static int x9 = p.get("maxHeightDeck");
    private static int x10 = p.get("heightCardBig");
    private static int x11 = p.get("maxWidthCard");
    private static ReentrantLock reentrantLock = new ReentrantLock();
    private static ReentrantLock reentrantLock1 = new ReentrantLock();
    private static ReentrantLock reentrantLock2 = new ReentrantLock();

    public static ArrayList<UIDoubleImage> findUIDoubleImage(ArrayList<UIDoubleImage> uiDoubleImages, ArrayList<Card> cards) {
        ArrayList<UIDoubleImage> uiObjects = new ArrayList<>(uiDoubleImages);
        ArrayList<UIDoubleImage> manager = new ArrayList<>();
        for (Card card : cards) {
            for (UIDoubleImage uiDoubleImage : uiObjects) {
                if (card.equals(uiDoubleImage.getCard())) {
                    manager.add(uiDoubleImage);
                    uiObjects.remove(uiDoubleImage);
                    break;
                }
            }
        }
        return manager;
    }

    public synchronized static void changeMinionTexture(Minion minion, String name, int i) {
        reentrantLock1.lock();
        ImageLoader.writeOnMinionImage(ImageLoader.loadImage("/texture/Minion/" + minion.getName() + "1.png"),
                minion, Integer.toString(minion.getHP()), Integer.toString(minion.getAttack()), name, i, 1, "Minions");
        ImageLoader.writeOnMinionImage(ImageLoader.loadImage("/texture/Minion/" + minion.getName() + "2.png"),
                minion, Integer.toString(minion.getHP()), Integer.toString(minion.getAttack()), name, i, 2, "Minions");
        String[] s = new String[]{"/texture/Minions during play/" + minion.getName() + "1_" + name + "_" + i + ".png"
                , "/texture/Minions during play/" + minion.getName() + "2_" + name + "_" + i + ".png"};
        minion.setMinionTexture(s);
        reentrantLock1.unlock();

    }

    public synchronized static void changeWeaponTexture(Weapon weapon, String name, int i) {
        reentrantLock.lock();
        ImageLoader.writeOnMinionImage(ImageLoader.loadImage("/texture/weapon/" + weapon.getName() + "1.png"),
                weapon, Integer.toString(weapon.getDurability()), Integer.toString(weapon.getAttack()), name, i, 1, "weapons");
        ImageLoader.writeOnMinionImage(ImageLoader.loadImage("/texture/weapon/" + weapon.getName() + "2.png"),
                weapon, Integer.toString(weapon.getDurability()), Integer.toString(weapon.getAttack()), name, i, 2, "weapons");
        String[] s = new String[]{"/texture/weapons during play/" + weapon.getName() + "1_" + name + "_" + i + ".png"
                , "/texture/weapons during play/" + weapon.getName() + "2_" + name + "_" + i + ".png"};
        weapon.setWeaponTexture(s);
        reentrantLock.unlock();
    }

    public synchronized static void changeHeroTexture(Hero hero, String name, int i) {
        reentrantLock2.lock();
        ImageLoader.writeOnHeroImage(ImageLoader.loadImage("/texture/heroes/" + hero.getHeroClass().toString() + "1.png"),
                hero, Integer.toString(hero.getHP()), name, i, 1);
        ImageLoader.writeOnHeroImage(ImageLoader.loadImage("/texture/heroes/" + hero.getHeroClass().toString() + "2.png"),
                hero, Integer.toString(hero.getHP()), name, i, 2);
        String[] s = new String[]{"/texture/heroes during play/" + hero.getHeroClass().toString() + "1_" + name + "_" + i + ".png"
                , "/texture/heroes during play/" + hero.getHeroClass().toString() + "2_" + name + "_" + i + ".png"};
        hero.setHeroTexture(s);
        reentrantLock2.unlock();
    }

    public static void arrangeCardDeck(ArrayList<UIDoubleImage> uiDoubleImages, String n, int x22) {
        int cardWidth = 0;
        if (uiDoubleImages.size() != 0)
            cardWidth = x8 / Math.min(uiDoubleImages.size(), 6) - (3 * uiDoubleImages.size() + 1);
        cardWidth = Math.min(cardWidth, x11);
        int height = (int) (cardWidth * Constants.HEIGHT_DIVIDED_BY_WIDTH);
        float y = (float) ((x9 - height) / 2.0 + x22);
        float x = (float) (Constants.COMPUTER_WIDTH / 2.0 - (uiDoubleImages.size() / 2.0) * (cardWidth + 2));
        float x2 = (float) (Constants.COMPUTER_WIDTH / 2.0 - (uiDoubleImages.size() / 2.0) * (cardWidth + 2) - cardWidth / 2);
        float y2;
        if (n.equals("player1") || n.equals("PLAYER1"))
            y2 = y - x10;
        else y2 = y + height - 10;
        for (UIDoubleImage uiDoubleImage : uiDoubleImages) {
            uiDoubleImage.setX(x);
            uiDoubleImage.setY(y);
            uiDoubleImage.setX2(x2);
            uiDoubleImage.setY2(y2);
            uiDoubleImage.setWidth(cardWidth);
            uiDoubleImage.setHeight(height);
            x += cardWidth + 5;
            x2 += cardWidth + 5;
        }
    }

    public static void drawMana(String prompt, Graphics2D g2D, String name, int x8) {
        Font font = new Font("Helvetica", Font.BOLD, 20);
        if (name.equals("player1") || name.equals("player2"))
            g2D.setColor(Color.BLUE);
        else g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(prompt, x7, x8);
    }

    public static void drawNumberCardInDeck(String prompt, Graphics2D g2D, String name, int x6) {
        Font font = new Font("Helvetica", Font.BOLD, 20);
        if (name.equals("player1") || name.equals("player2"))
            g2D.setColor(Color.BLUE);
        else g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(prompt, x5, x6);
    }

    public static void drawTimer(String timer, Graphics2D g2D) {
        Font font = new Font("Helvetica", Font.BOLD, 50);
        g2D.setColor(Color.GREEN);
        g2D.setFont(font);
        g2D.drawString(timer, x3, x4);
    }

    public static void drawDevelopmentPercent(String percent, Graphics2D g2D, int x) {
        Font font = new Font("Helvetica", Font.BOLD, 50);
        g2D.setColor(Color.ORANGE);
        g2D.setFont(font);
        g2D.drawString(percent, x1, x);
    }
}
