package states.playState;

import Entity.cards.Card;
import Entity.cards.minion.Minion;
import Entity.cards.weapon.Weapon;
import Entity.hero.Hero;
import constants.Constants;
import gfx.Asserts;
import gfx.Drawer;
import gfx.ImageLoader;
import main.Handler;
import otherModels.OtherModels;
import otherModels.configModel.PlayStateModel;
import states.collectionState.CardInitUtils;
import states.collectionState.CardNameInitUtils;
import states.collectionState.HeroNameInitUtils;
import ui.UICardImage;
import ui.UIDoubleImage;
import ui.UIManager;
import ui.UIRecImage;

import java.awt.*;
import java.util.ArrayList;

public class GamePlayer {
    private MapperPlay mapperPlay;
    private Handler handler;
    private String name;
    private ArrayList<UICardImage> containLandUI;
    private ArrayList<UIDoubleImage> containDeckUI;
    private UIManager uiManagerLand;
    private UIManager uiManagerDeck;
    private UIManager uiManagerHero;
    private int controllerCounter = 0;
    private boolean controller = false;
    private Minion MinionWantSummon;
    private int helperSavingMinionTexture = 0;
    private int helperSavingHeroTexture = 0;
    private int counterPlay = 0;
    private boolean done;
    private int doneCounter;
    private PlayModel playModel;
    private int x1;
    private int x2;
    private int x3;
    private int x4;
    private int x5;
    private int x6;

    public GamePlayer(Handler handler, String name, PlayModel playModel) {
        this.handler = handler;
        this.name = name;
        this.playModel = playModel;
        mapperPlay = new MapperPlay(name);
        x1 = mapperPlay.get("initialYPosEvent");
        x2 = mapperPlay.get("horizontalDistanceEvent");
        x3 = mapperPlay.get("initialYPosDevelopmentPercent" + name.charAt(6));
        x4 = mapperPlay.get("initialYPosNumberCardInDeck" + name.charAt(6));
        x5 = mapperPlay.get("initialYPosMana" + name.charAt(6));
        x6 = mapperPlay.get("minInitialYPos" + name.charAt(6));
        mapperPlay.updateParameter();
        if (name.equals("player1") || name.equals("player2"))
            playModel.setGamePlayer(this);
        else playModel.setEnemyPlayer(this);
        containDeckUI = new ArrayList<>();
        containLandUI = new ArrayList<>();
        uiManagerDeck = new UIManager(handler);
        uiManagerLand = new UIManager(handler);
        uiManagerHero = new UIManager(handler);
        cardDeckInitFirst();
        landInit();
        cardLandInitFirst();
        cardDeckInit();
        heroInit();
        heroPowerInit();
        updateHero();
        weaponInit();
        new Thread(() -> {
            while (true) {
                mapperPlay.updateParameter();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (playModel.isStop())
                    break;
            }
        }).start();
    }

    public void tick() {
        if (controller) {
            controllerCounter++;
            if (controllerCounter == 20) {
                controllerCounter = 0;
                controller = false;
            }
        }
        uiManagerDeck.tick();
        uiManagerLand.tick();
        uiManagerHero.tick();
    }

    public void render(Graphics2D g2D) {
        int mana;
        int level;
        int numberOfCards;
        String errorMassage;
        ArrayList<String> events;
        if (name.equals("player1") || name.equals("player2")) {
            mana = playModel.getMana();
            level = playModel.getLevel();
            numberOfCards = playModel.getNumberOfCards();
            errorMassage = playModel.getErrorMassage();
            events = playModel.getEvent();
        } else {
            mana = playModel.getMana1();
            level = playModel.getLevel1();
            numberOfCards = playModel.getNumberOfCards1();
            errorMassage = playModel.getErrorMassage1();
            events = playModel.getEvent1();
        }
        GraphicUtils.drawMana("Mana: " + mana + "/" + level, g2D, name, x5);
        GraphicUtils.drawNumberCardInDeck("Number of cards: " + numberOfCards, g2D, name, x4);
        drawPercent(g2D);
        uiManagerLand.render(g2D);
        uiManagerHero.render(g2D);
        uiManagerDeck.render(g2D);
        int height = x1;
        if (events != null)
            if (events.size() > 0)
                for (String s : events) {
                    drawer(g2D, s, height);
                    height += x2;
                }
        if (playModel.getErrorMassage() != null) {
            ErrorMassagePlay(g2D, errorMassage);
        }
        if (playModel.getEndMassage() != null) {
            Drawer.endMassage(g2D, playModel.getEndMassage());
        }
        if (playModel.getConnection() != null) {
            Drawer.endMassage(g2D, playModel.getConnection());
        }
    }

    private void drawPercent(Graphics2D g2D) {
        boolean questRewardNull;
        String developmentPercent;
        if (name.equals("player1") || name.equals("player2")) {
            questRewardNull = playModel.isQuestRewardNull();
            developmentPercent = playModel.getDevelopmentPercent();
        } else {
            questRewardNull = playModel.isQuestRewardNull1();
            developmentPercent = playModel.getDevelopmentPercent1();
        }
        if (developmentPercent == null)
            developmentPercent = "";
        if (!questRewardNull) {
            GraphicUtils.drawDevelopmentPercent(developmentPercent, g2D, x3);
        } else if (done && doneCounter < 100) {
            GraphicUtils.drawDevelopmentPercent("done", g2D, x3);
            doneCounter++;
        } else if (doneCounter == 100) {
            doneCounter = 0;
            done = false;
        }
    }

    private void cardDeckInitFirst() {
        Card card;
        for (int i = 0; i < mapperPlay.getDeckCardSize() || mapperPlay.getExtraCard() != null; i++) {
            if (i < mapperPlay.getDeckCardSize()) {
                card = mapperPlay.getDeckCard(i);
            } else {
                card = mapperPlay.getExtraCard();
            }
            Card finalCard = card;
            containDeckUI.add(new UIDoubleImage(card, mapperPlay.getDeckCardSize(), 0, 0, 0, 0, 0, mapperPlay.get("widthCardBig"),
                    mapperPlay.get("heightCardBig"), ImageLoader.loadDoubleImage(card.getTexturePath()[0], card.getTexturePath()[0]), () -> {
                if (name.equals("player1") || name.equals("player2") || handler.getCollectionState().getConstantButton().isTraining()) {
                    if (controllerCounter == 0 && mapperPlay.isTurn()) {
                        mapperPlay.clickDeckCard(finalCard);
                        mapperPlay.changeUpdateFlagDeck();
                        controllerCounter++;
                        controller = true;
                    }
                }
            }));
            mapperPlay.changeExtraCard();
        }
        cardDeckInit();
//        mapperPlay.changeUpdateFlagDeck();//todo
    }

    public void cardDeckInitBad() {
        ArrayList<UIDoubleImage> manager;
        manager = GraphicUtils.findUIDoubleImage(containDeckUI, mapperPlay.getContainDeckCard());
        GraphicUtils.arrangeCardDeck(manager, name, x6);
        uiManagerDeck.setButtons(CardNameInitUtils.toUIObject(manager));
    }

    public void cardDeckInit() {
        cardDeckInitBad();
        cardDeckInitBad();
    }

    public void landInit() {
        int height = mapperPlay.get("maxHeightLand");
        int width = (int) (height / Constants.HEIGHT_DIVIDED_BY_WIDTH);
        float y = mapperPlay.get("minInitialYPosLand" + name.charAt(6));
        float x = (float) (Constants.COMPUTER_WIDTH / 2.0 - (7 / 2.0) * (width + 2));
        for (int i = 0; i < 7; i++) {
            int finalI = i;
            containLandUI.add(new UICardImage(null, x, y, width, height,
                    Asserts.land, () -> {
                if (containLandUI.get(finalI).getCard() == null && mapperPlay.getMinionWantedSummon() != null) {
                    summonMinion(finalI);
                } else if (containLandUI.get(finalI).getCard() != null) {
                    mapperPlay.clickLandCard(finalI);
                }
            }));
            x += width + 3;
        }
    }

    public void summonMinion(int i) {
        MinionWantSummon = mapperPlay.getMinionWantedSummon();
        OtherModels.summonMinion(i);
        containLandUI.get(i).setCard(MinionWantSummon);
        containLandUI.get(i).setImages(ImageLoader.loadDoubleImage(MinionWantSummon.getTexturePath()[0], MinionWantSummon.getTexturePath()[7]));
        mapperPlay.removeDeckMinion(MinionWantSummon, i);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mapperPlay.changeUpdateFlagDeck();
    }

    public void otherSummonMinion(int i) {
        MinionWantSummon = mapperPlay.getMinionWantedSummon();
        containLandUI.get(i).setCard(MinionWantSummon);
        containLandUI.get(i).setImages(ImageLoader.loadDoubleImage(MinionWantSummon.getTexturePath()[0], MinionWantSummon.getTexturePath()[7]));
    }

    public void updateHP(int i) {
        mapperPlay.checkHPLandCards(i);
        Minion[] minions = mapperPlay.getContainLandCard();
        if (minions[i] != null) {
            GraphicUtils.changeMinionTexture(minions[i], name, helperSavingMinionTexture);
            helperSavingMinionTexture++;
            containLandUI.get(i).setCard(minions[i]);
            if (minions[i].getMinionTexture() == null)
                containLandUI.get(i).setImages(ImageLoader.loadDoubleImage(minions[i].getTexturePath()[0], minions[i].getTexturePath()[7]));
            else
                containLandUI.get(i).setImages(ImageLoader.loadDoubleImage(minions[i].getMinionTexture()[0], minions[i].getMinionTexture()[1]));
        } else {
            containLandUI.get(i).setCard(null);
            containLandUI.get(i).setImages(Asserts.land);
        }
    }

    public void updateWeapon() {
        mapperPlay.checkDurability();
        Weapon weapon = mapperPlay.getWeapon();
        if (weapon != null) {
            GraphicUtils.changeWeaponTexture(weapon, name, 1);
            if (mapperPlay.getWeapon().getWeaponTexture() == null)
                uiManagerHero.getButtons().get(2).setImages(ImageLoader.loadDoubleImage(weapon.getTexturePath()[0], weapon.getTexturePath()[7]));
            else
                uiManagerHero.getButtons().get(2).setImages(ImageLoader.loadDoubleImage(weapon.getWeaponTexture()[0], weapon.getWeaponTexture()[1]));
        } else {
            uiManagerHero.getButtons().get(2).setImages(Asserts.land);
        }
    }

    public void minionAttack() {
        updateHP(mapperPlay.getIndexMinionWantAttack());
    }

    public void specialUpdate() {
        for (int i = 0; i < 7; i++) {
            updateHP(i);
        }
    }

    public void updateHero() {
        Hero hero = mapperPlay.getHero();
        GraphicUtils.changeHeroTexture(hero, name, helperSavingHeroTexture);
        helperSavingHeroTexture++;
        uiManagerHero.getButtons().get(0).setImages(ImageLoader.loadDoubleImage(hero.getHeroTexture()[0], hero.getHeroTexture()[1]));
    }

    public void cardLandInitFirst() {
        uiManagerLand.setButtons(CardInitUtils.toUIObject(containLandUI));
    }

    public void heroInit() {
        uiManagerHero.addButton(new UIRecImage(mapperPlay.get("initialXPosHero"), mapperPlay.get("initialYPosHero" + name.charAt(6)), mapperPlay.get("widthHero"), mapperPlay.get("heightHero"),
                Asserts.heroesPlay[HeroNameInitUtils.recognizeHero(mapperPlay.getHeroClass()) - 1], () -> {
            mapperPlay.clickOnHero();
        }
        ));
    }

    public void heroPowerInit() {
        uiManagerHero.addButton(new UIRecImage(mapperPlay.get("initialXPosHeroPower"), mapperPlay.get("initialYPosHeroPower" + name.charAt(6)), mapperPlay.get("widthHeroPower"), mapperPlay.get("heightHeroPower"),
                Asserts.heroesHeroPowerPlay[HeroNameInitUtils.recognizeHero(mapperPlay.getHeroClass()) - 1], () -> {
            if (name.equals("player1") || name.equals("player2") || handler.getCollectionState().getConstantButton().isTraining())
                mapperPlay.heroPower();
        }
        ));
    }

    public void weaponInit() {
        uiManagerHero.addButton(new UIRecImage(mapperPlay.get("initialXPosWeapon"), mapperPlay.get("initialYPosWeapon" + name.charAt(6)), mapperPlay.get("widthWeapon"), mapperPlay.get("heightWeapon"),
                Asserts.land, () -> {
            if (name.equals("player1") || name.equals("player2") || handler.getCollectionState().getConstantButton().isTraining())
                mapperPlay.weaponAttack();
        }));
    }

    private void drawer(Graphics2D g2D, String prompt, int height) {
        Font font = new Font("Helvetica", Font.BOLD, 10);
        if (name.equals("player1") || name.equals("player2"))
            g2D.setColor(Color.BLUE);
        else g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString(prompt, mapperPlay.get("initialXPosEvent" + name.charAt(6)), height);
    }

    private void ErrorMassagePlay(Graphics2D g2D, String error) {
        boolean error1;
        if (name.equals("player1") || name.equals("player2"))
            error1 = playModel.isError();
        else error1 = playModel.isError1();
        if (error1) {
            counterPlay++;
            Drawer.Massage(error, g2D, name);
            if (counterPlay == 100) {
                mapperPlay.changeError();
                counterPlay = 0;
                mapperPlay.changeErrorMassage();
            }
        }
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public MapperPlay getMapperPlay() {
        return mapperPlay;
    }

    public String getName() {
        return name;
    }

    public void setDone(boolean doneCounter) {
        this.done = doneCounter;
    }

    public PlayModel getPlayModel() {
        return playModel;
    }
}
