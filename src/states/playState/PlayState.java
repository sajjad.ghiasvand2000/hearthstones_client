package states.playState;

import constants.Constants;
import gfx.Asserts;
import main.Handler;
import main.MainClient;
import states.State;
import states.playState.constantButton.ConstantButton;

import java.awt.*;
import java.io.IOException;

public class PlayState extends State {
    private ConstantButton constantButton;
    private GamePlayer gamePlayer;
    private GamePlayer enemyPlayer;
    private GamePlayer currentGamePlayer;
    private String name;
    private PlayModel playModel;

    public PlayState(Handler handler) {
        super(handler);
        try {
            MainClient.getDataOutputStream().writeUTF("name_hello");
            name = MainClient.getDataInputStream().readUTF();
            System.out.println(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
        char x ;
        if (name.charAt(6) == '1')
            x = '2';
        else x = '1';
        playModel = new PlayModel();
        playModel.start();
        gamePlayer = new GamePlayer(handler, name, playModel);
        enemyPlayer = new GamePlayer(handler, "PLAYER" + x, playModel);
        if (gamePlayer.getName().equals("player1"))
            currentGamePlayer = gamePlayer;
        else currentGamePlayer = enemyPlayer;
        constantButton = new ConstantButton(handler);
    }

    @Override
    public void tick() {
        constantButton.tick();
        gamePlayer.tick();
        enemyPlayer.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.playBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        constantButton.render(g2D);
        if (gamePlayer.getName().equals("player1")) {
            gamePlayer.render(g2D);
            enemyPlayer.render(g2D);
        }else {
            enemyPlayer.render(g2D);
            gamePlayer.render(g2D);
        }

    }

    public void changeGamePlayer() {
        if (currentGamePlayer.getName().equals("player1") || currentGamePlayer.getName().equals("player2")) {
            currentGamePlayer = enemyPlayer;
        } else {
            currentGamePlayer = gamePlayer;
        }
    }

    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }

    public GamePlayer getCurrentGamePlayer() {
        return currentGamePlayer;
    }

    public ConstantButton getConstantButton() {
        return constantButton;
    }

    public GamePlayer getEnemyPlayer() {
        return enemyPlayer;
    }
}