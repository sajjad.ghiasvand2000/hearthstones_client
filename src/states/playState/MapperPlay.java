package states.playState;

import Entity.Deck;
import Entity.EntityViewModel;
import Entity.cards.Card;
import Entity.cards.Type;
import Entity.cards.minion.Minion;
import Entity.cards.spell.Spell;
import Entity.cards.weapon.Weapon;
import Entity.hero.Hero;
import Entity.hero.HeroClass;
import Entity.viewModel.*;
import main.MainClient;

import java.io.IOException;
import java.util.ArrayList;

public class MapperPlay {

    private EntityViewModel entityViewModel;
    private String name;

    public MapperPlay(String name) {
        entityViewModel = new EntityViewModel();
        this.name = name;
    }

    public synchronized void clickEndTurn() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_clickEndTurn");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void addCardToHand() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_addCardToHand");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void clickDeckCard(Card card) {
        try {
            if (card.getType().equals(Type.SPELL)) {
                MainClient.getDataOutputStream().writeUTF(name + "_" + "play_clickDeckCard_spell");
                MainClient.getObjectOutputStream().writeObject(entityViewModel.setSpell((Spell) card));
            } else if (card.getType().equals(Type.MINION)) {

                MainClient.getDataOutputStream().writeUTF(name + "_" + "play_clickDeckCard_minion");
                MainClient.getObjectOutputStream().writeObject(entityViewModel.setMinion((Minion) card));
            } else if (card.getType().equals(Type.WEAPON)) {
                MainClient.getDataOutputStream().writeUTF(name + "_" + "play_clickDeckCard_weapon");
                MainClient.getObjectOutputStream().writeObject(entityViewModel.setWeapon((Weapon) card));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized ArrayList<Card> getContainDeckCard() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_" + "getContainDeckCard");
            MainClient.getDataOutputStream().flush();
            ArrayList<Card> cards = new ArrayList<>();
            ArrayList<Minion> minions = entityViewModel.getMinions((ArrayList<MinionViewModel>) MainClient.getObjectInputStream().readObject());
            ArrayList<Spell> spells = entityViewModel.getSpells((ArrayList<SpellViewModel>) MainClient.getObjectInputStream().readObject());
            ArrayList<Weapon> weapons = entityViewModel.getWeapons((ArrayList<WeaponViewModel>) MainClient.getObjectInputStream().readObject());
            cards.addAll(minions);
            cards.addAll(spells);
            cards.addAll(weapons);
            return cards;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized Deck getDeck() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getDeck");
            MainClient.getDataOutputStream().flush();
            return entityViewModel.getDeck((DeckViewModel) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized Minion getMinionWantedSummon() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getMinionWantedSummon");
            MainClient.getDataOutputStream().flush();
            return entityViewModel.getMinion((MinionViewModel) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }


    public synchronized void removeDeckMinion(Minion minion, int i) {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_removeDeckMinion_" + i);
            MainClient.getDataOutputStream().flush();
            MainClient.getObjectOutputStream().writeObject(entityViewModel.setMinion(minion));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void clickLandCard(int i) {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_clickLandCard_" + i);
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public synchronized Minion[] getContainLandCard() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getContainLandCard");
            MainClient.getDataOutputStream().flush();
            return entityViewModel.getArrayMinions((MinionViewModel[]) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized void makeMinionsAttackable() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_makeMinionsAttackable");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public synchronized boolean isTurn() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_isTurn");
            MainClient.getDataOutputStream().flush();
            return Boolean.parseBoolean(MainClient.getDataInputStream().readUTF());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


    public synchronized void checkHPLandCards(int i) {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_checkHPLandCards_" + i);
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void heroPower() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_heroPower");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized int getIndexMinionWantAttack() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getIndexMinionWantAttack");
            MainClient.getDataOutputStream().flush();
            return Integer.parseInt(MainClient.getDataInputStream().readUTF());
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public synchronized Hero getHero() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getHero");
            MainClient.getDataOutputStream().flush();
            return entityViewModel.getHero((HeroViewModel) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized HeroClass getHeroClass() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getHeroClass");
            MainClient.getDataOutputStream().flush();
            return (HeroClass) MainClient.getObjectInputStream().readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized Card getDeckCard(int i) {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getDeckCard_" + i);
            MainClient.getDataOutputStream().flush();
            String type = MainClient.getDataInputStream().readUTF();
            return helper(type);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized int getDeckCardSize() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getDeckCardSize");
            MainClient.getDataOutputStream().flush();
            return Integer.parseInt(MainClient.getDataInputStream().readUTF());
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public synchronized void changeUpdateFlagDeck() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_changeUpdateFlagDeck");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized Card getExtraCard() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getExtraCard");
            MainClient.getDataOutputStream().flush();
            String type = MainClient.getDataInputStream().readUTF();
            return helper(type);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized void changeExtraCard() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_changeExtraCard");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void clickOnHero() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_clickOnHero");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void changeError() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_changeError");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void changeErrorMassage() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_changeErrorMassage");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized Weapon getWeapon() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_getWeapon");
            MainClient.getDataOutputStream().flush();
            return entityViewModel.getWeapon((WeaponViewModel) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized void weaponAttack() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_weaponAttack");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void checkDurability() {
        try {
            MainClient.getDataOutputStream().writeUTF(name + "_" + "play_checkDurability");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Card helper(String type) throws ClassNotFoundException, IOException{
        switch (type) {
            case "minion":
                return entityViewModel.getMinion((MinionViewModel) MainClient.getObjectInputStream().readObject());
            case "spell":
                return entityViewModel.getSpell((SpellViewModel) MainClient.getObjectInputStream().readObject());
            case "weapon":
                return entityViewModel.getWeapon((WeaponViewModel) MainClient.getObjectInputStream().readObject());
        }
        return null;
    }

    public synchronized int get(String s){
        try {
            MainClient.getDataOutputStream().writeUTF("config_play_"+ s);
            MainClient.getDataOutputStream().flush();
            return Integer.parseInt(MainClient.getDataInputStream().readUTF());
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public synchronized void updateParameter(){
        try {
            MainClient.getDataOutputStream1().writeUTF(name + "_" + "play_updateParameter");
            MainClient.getDataOutputStream1().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
