package states.playState.constantButton;

import main.Handler;
import main.MainClient;
import otherModels.OtherModels;
import states.playState.GamePlayer;
import ui.ClickListener;
import java.awt.*;

public class EndTurn implements ClickListener {
    private Handler handler;
    private Timer timer;

    public EndTurn(Handler handler) {
        this.handler = handler;
        timer = new Timer(handler);
    }

    public void render(Graphics2D g2D){
        timer.render(g2D);
    }

    @Override
    public void onClick() {
        if (handler.getPlayState().getCurrentGamePlayer().getName().equals("player1") || handler.getPlayState().getCurrentGamePlayer().getName().equals("player2") ||
                handler.getCollectionState().getConstantButton().isTraining()) {
            OtherModels.endTurnClicked();
            OtherModels.changeGamePlayer();
            OtherModels.body("click end turn", "click end turn in play state");
            timer.setOnClickRequired(true);
            timer = new Timer(handler);
            timer.start();
            handler.getPlayState().getCurrentGamePlayer().getMapperPlay().clickEndTurn();
            handler.getPlayState().getCurrentGamePlayer().getMapperPlay().makeMinionsAttackable();
            handler.getPlayState().changeGamePlayer();
            GamePlayer gamePlayer = handler.getPlayState().getCurrentGamePlayer();
            gamePlayer.getMapperPlay().addCardToHand();
            gamePlayer.getMapperPlay().changeUpdateFlagDeck();
            if (gamePlayer.getPlayModel().isStop()){
                timer.interrupt();
            }
        }
    }

    public Timer getTimer() {
        return timer;
    }

    public void newTimer(){
        timer.setOnClickRequired(true);
        timer = new Timer(handler);
        timer.start();
    }
}
