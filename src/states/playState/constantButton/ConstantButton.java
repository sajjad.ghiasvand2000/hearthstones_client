package states.playState.constantButton;


import gfx.Asserts;
import gfx.Drawer;
import gfx.Input;
import main.Handler;
import main.Util;
import otherModels.OtherModels;
import otherModels.configModel.CollectionStateModel;
import otherModels.configModel.MenuStateModel;
import otherModels.configModel.PlayStateModel;
import states.State;
import states.playState.GamePlayer;
import states.playState.PlayModel;
import ui.UIManager;
import ui.UIRecImage;
import java.awt.*;
import java.awt.image.BufferedImage;

public class ConstantButton {
    private PlayStateModel p = new PlayStateModel();
    private static MenuStateModel m = new MenuStateModel();
    private static CollectionStateModel c = new CollectionStateModel();
    private BufferedImage[] bufferedImages;
    private Input massageInput;
    private Handler handler;
    private UIManager uiManager;
    private GamePlayer gamePlayer;
    private EndTurn endTurn;
    int x1 = p.get("initialXPosSearchStringReceiveMassage");
    int x2 = p.get("initialYPosSearchString");

    public ConstantButton(Handler handler) {
        this.handler = handler;
        uiManager = new UIManager(handler);
        endTurn = new EndTurn(handler);
        massageInput = new Input(handler, 0, 0, 0, 30);
        bufferedImages = new BufferedImage[]{Asserts.search[1], Asserts.search[1]};
        menu();
        exit();
        endTurn();
        endTurn.getTimer().start();
        if (!handler.getCollectionState().getConstantButton().isTraining()) {
            massage();
            massageIcon();
            receiveMassage();
        }
    }

    public void tick() {
        uiManager.tick();
        if (massageInput != null)
            massageInput.tick();
    }

    public void render(Graphics2D g2D) {
        uiManager.render(g2D);
        endTurn.render(g2D);
        if (massageInput != null)
            massageInput.render(g2D);
        Drawer.drawString(g2D, PlayModel.getMassage(), 15, x1, x2);
    }

    private void menu() {
        uiManager.addButton(new UIRecImage(m.get("initialXPosMenu"), m.get("initialYPosMenu"),
                m.get("widthMenu"), m.get("heightMenu"), Asserts.menu, () -> {
            OtherModels.body("click menu button", "back to menu sate");
            handler.getCollectionState().getConstantButton().setDeckReader(false);
            handler.getCollectionState().getConstantButton().setTraining(false);
            State.setCurrentState(handler.getMenuState());
        }));
    }

    private void endTurn() {
        uiManager.addButton(new UIRecImage(p.get("initialXPos"), p.get("initialYPos"), p.get("widthEndTurn"),
                p.get("heightEndTurn"), Asserts.endTurn, endTurn));
    }

    private void massage() {
        uiManager.addButton(new UIRecImage(p.get("initialXPosSearch"), p.get("initialYPosSearch"), c.get("widthSearch"),
                c.get("heightSearch"), bufferedImages, () -> {
                    OtherModels.body("click search", "write for massaging");
                    uiManager.getButtons().get(3).setImages(bufferedImages);
                    massageInput = new Input(handler, 40, p.get("initialXPosSearchString"), p.get("initialYPosSearchString"), 15);
                }));
    }

    private void massageIcon() {
        uiManager.addButton(new UIRecImage(p.get("initialXPosSearchIcon"), p.get("initialYPosSearchIcon"),
                c.get("widthSearchIcon"), c.get("heightSearchIcon"), Asserts.searchIcon, () -> {
            OtherModels.body("click searchIcon", "click for massaging");
            uiManager.getButtons().get(3).setImages(bufferedImages);
            OtherModels.massage(massageInput.getInput());
            massageInput = new Input(handler, 0, 0, 0, 15);
        }));
    }

    private void receiveMassage() {
        uiManager.addButton(new UIRecImage(p.get("initialXPosReceiveMassage"), p.get("initialYPosSearch"), c.get("widthSearch"),
                c.get("heightSearch"), bufferedImages, () -> {
        }));
    }

    private void exit() {
        uiManager.addButton(new UIRecImage(m.get("initialXPosMenu") + m.get("widthMenu") +
                m.get("horizontalDistance"), m.get("initialYPosMenu"), m.get("widthMenu"),
                m.get("heightMenu"), Asserts.exit, () -> {
            OtherModels.body("exit", "exit");
            OtherModels.saveChanges();
            OtherModels.exitInPlay();
            System.exit(0);
        }));
    }

    public EndTurn getEndTurn() {
        return endTurn;
    }

}
