package states.playState;

import login.LoginPanel;
import main.MainClient;
import states.State;
import states.connectionState.ConnectionState;

import java.io.IOException;
import java.util.ArrayList;

public class PlayModel extends Thread {
    private GamePlayer gamePlayer;
    private GamePlayer enemyPlayer;
    private int mana;
    private int mana1;
    private int level;
    private int level1;
    private int numberOfCards;
    private int numberOfCards1;
    private String errorMassage;
    private String errorMassage1;
    private boolean questRewardNull;
    private boolean questRewardNull1;
    private String developmentPercent;
    private String developmentPercent1;
    private boolean error;
    private boolean error1;
    private ArrayList<String> event;
    private ArrayList<String> event1;
    private static String massage = "";
    private String endMassage;
    private String connection;
    private boolean stop;

    public PlayModel() {
        event = new ArrayList<>();
    }

    @Override
    public void run() {
        while (true) {
            String request;
            try {
                request = MainClient.getDataInputStream1().readUTF();
                String[] data = request.split("_");
                if (data[0].equals(gamePlayer.getName())) {
                    events(gamePlayer, data);
                } else events(enemyPlayer, data);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void events(GamePlayer gamePlayer1, String[] data) throws IOException, ClassNotFoundException {
        switch (data[1]) {
            case "statusNet":
                if (!Boolean.parseBoolean(data[2])) {
                    states.State.setCurrentState(new ConnectionState(gamePlayer1.getHandler()));
                } else states.State.setCurrentState(gamePlayer1.getHandler().getMenuState());
                break;
            case "updateParameter":
                if (data[0].equals(gamePlayer.getName())) {
                    mana = Integer.parseInt(data[2]);
                    level = Integer.parseInt(data[3]);
                    numberOfCards = Integer.parseInt(data[4]);
                    questRewardNull = Boolean.parseBoolean(data[5]);
                    developmentPercent = data[6];
                    errorMassage = data[7];
                    error = Boolean.parseBoolean(data[8]);
//                    event = (ArrayList<String>) MainClient.getObjectInputStream1().readObject();
                } else {
                    mana1 = Integer.parseInt(data[2]);
                    level1 = Integer.parseInt(data[3]);
                    numberOfCards1 = Integer.parseInt(data[4]);
                    questRewardNull1 = Boolean.parseBoolean(data[5]);
                    developmentPercent1 = data[6];
                    errorMassage1 = data[7];
                    error1 = Boolean.parseBoolean(data[8]);
//                    event1 = (ArrayList<String>) MainClient.getObjectInputStream1().readObject();
                }
                break;
            case "endGame":
                if (data[2].equals("looser")) {
                    endMassage = "You lose the game!";
                } else if (data[2].equals("winner")){
                    endMassage = "You won the game!";
                }else {
                    endMassage = "Game Finished!";
                }
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                states.State.setCurrentState(gamePlayer1.getHandler().getMenuState());
                stop = true;
                break;
            case "poorEnemyConnection":
                if (data[2].equals("null"))
                    connection = null;
                else if (data[2].equals("Your enemy is unconnected"))
                    connection = data[2];
                break;
            case "minionAttack":
                gamePlayer1.minionAttack();
                break;
            case "updateHP":
                gamePlayer1.updateHP(Integer.parseInt(data[2]));
                break;
            case "updateHero":
                gamePlayer1.updateHero();
                break;
            case "updateWeapon":
                gamePlayer1.updateWeapon();
                break;
            case "specialUpdate":
                gamePlayer1.specialUpdate();
                break;
            case "cardDeckInit":
                gamePlayer1.cardDeckInit();
                break;
            case "cardDeckInitFirst":
                gamePlayer1.cardLandInitFirst();
                break;
            case "massage":
                massage = data[2];
                break;
            case "endTurnClicked":
                gamePlayer1.getHandler().getPlayState().getConstantButton().getEndTurn().newTimer();
                gamePlayer1.getHandler().getPlayState().changeGamePlayer();
                break;
            case "summonMinion":
                enemyPlayer.otherSummonMinion(Integer.parseInt(data[2]));
                break;
            case "done":
                gamePlayer1.setDone(true);
                break;
            case "getEvent":
                event = (ArrayList<String>) MainClient.getObjectInputStream1().readObject();
                if (event == null) event = new ArrayList<>();
                break;
        }
    }

    public int getMana() {
        return mana;
    }

    public int getMana1() {
        return mana1;
    }

    public int getLevel() {
        return level;
    }

    public int getNumberOfCards() {
        return numberOfCards;
    }

    public String getErrorMassage() {
        return errorMassage;
    }

    public boolean isQuestRewardNull() {
        return questRewardNull;
    }

    public String getDevelopmentPercent() {
        return developmentPercent;
    }

    public boolean isError() {
        return error;
    }

    public ArrayList<String> getEvent() {
        return event;
    }

    public void setGamePlayer(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }

    public void setEnemyPlayer(GamePlayer enemyPlayer) {
        this.enemyPlayer = enemyPlayer;
    }

    public int getLevel1() {
        return level1;
    }

    public int getNumberOfCards1() {
        return numberOfCards1;
    }

    public String getErrorMassage1() {
        return errorMassage1;
    }

    public boolean isQuestRewardNull1() {
        return questRewardNull1;
    }

    public String getDevelopmentPercent1() {
        return developmentPercent1;
    }

    public boolean isError1() {
        return error1;
    }

    public static String getMassage() {
        return massage;
    }

    public String getEndMassage() {
        return endMassage;
    }

    public String getConnection() {
        return connection;
    }

    public ArrayList<String> getEvent1() {
        return event1;
    }

    public boolean isStop() {
        return stop;
    }
}
