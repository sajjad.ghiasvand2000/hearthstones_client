package states.connectionState;

import constants.Constants;
import main.Handler;
import states.State;
import java.awt.*;

public class ConnectionState extends State {
    public ConnectionState(Handler handler) {
        super(handler);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics2D g2D) {
        Font font = new Font("Helvetica", Font.BOLD, 60);
        FontMetrics fontMetrics = g2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth("connecting...");
        g2D.setColor(Color.RED);
        g2D.setFont(font);
        g2D.drawString("connecting...", (Constants.COMPUTER_WIDTH - width) / 2, (Constants.COMPUTER_HEIGHT - 60) / 2);
    }
}
