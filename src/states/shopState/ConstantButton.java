package states.shopState;

import gfx.Asserts;
import main.Handler;
import main.Util;
import otherModels.configModel.CollectionStateModel;
import otherModels.configModel.MenuStateModel;
import states.State;
import ui.UIManager;
import ui.UIRecImage;

import java.awt.*;

public class ConstantButton {
    private static CollectionStateModel c = new CollectionStateModel();
    private static MenuStateModel m = new MenuStateModel();
    private Handler handler;
    private UIManager uiManager;

    public ConstantButton(Handler handler) {
        this.handler = handler;
        uiManager = new UIManager(handler);
        nextPageButton();
        previousPageButton();
        sell();
        buy();
        menu();
        Util.exit(uiManager);
    }

    public void render(Graphics2D g2D) {
        uiManager.render(g2D);
    }

    public void tick() {
        uiManager.tick();
    }

    private void nextPageButton() {
        uiManager.addButton(new UIRecImage(1450, 380, 20, 20, Asserts.nextPage, () -> {
            if (handler.getShopState().getPage() == 1) {
                handler.getShopState().setPage(2);
            }
            handler.getShopState().cardInit();
        }));
    }

    private void previousPageButton() {
        uiManager.addButton(new UIRecImage(10, 380, 20, 20, Asserts.nextPage, () -> {
            if (handler.getShopState().getPage() == 2) {
                handler.getShopState().setPage(1);
            }
            handler.getShopState().cardInit();
        }));
    }

    private void buy() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosLockButton"), c.get("initialYPosLockButton"),
                c.get("widthLockButton"), c.get("heightLockButton"), Asserts.lock, () -> {
            handler.getShopState().setShowLockCards(true);
            handler.getShopState().setShowUnLockCards(false);
            handler.getShopState().cardInit();
        }));

    }

    private void sell() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosLockButton") + c.get("widthLockButton") +
                c.get("horizontalDistanceLockButton"), c.get("initialYPosLockButton"),
                c.get("widthLockButton"), c.get("heightLockButton"), Asserts.unlock, () -> {
            handler.getShopState().setShowLockCards(false);
            handler.getShopState().setShowUnLockCards(true);
            handler.getShopState().cardInit();
        }));
    }

    private void menu(){
        uiManager.addButton(new UIRecImage(m.get("initialXPosMenu"), m.get("initialYPosMenu"),
                m.get("widthMenu"), m.get("heightMenu"), Asserts.menu, () -> {
            State.setCurrentState(handler.getMenuState());
            handler.getCollectionState().updateCardInit();
        }) );
    }
}
