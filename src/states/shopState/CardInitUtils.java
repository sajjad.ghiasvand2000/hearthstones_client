package states.shopState;

import Entity.Deck;
import Entity.cards.Card;
import otherModels.configModel.CollectionStateModel;
import otherModels.configModel.ShopStateModel;
import ui.UICardImage;

import java.util.ArrayList;

public class CardInitUtils {
    private static ShopStateModel s = new ShopStateModel();
    private static CollectionStateModel c = new CollectionStateModel();

    public static void arrangeCards(ArrayList<UICardImage> uiCardImages) {
        int counter = 0;
        float xPosCard = s.get("initialXPosCard");
        float yPosCard = s.get("initialYPosCard");
        for (UICardImage uiCardImage : uiCardImages) {
            counter++;
            uiCardImage.setX(xPosCard);
            uiCardImage.setY(yPosCard);
            xPosCard += s.get("widthCard") + s.get("horizontalDistanceCard");
            if (counter == s.get("maxCardRow") || counter == s.get("maxCardRow")*2) {
                yPosCard += s.get("heightCard") + s.get("verticalDistanceCard");
                xPosCard -= (s.get("widthCard") + s.get("horizontalDistanceCard")) * s.get("maxCardRow");
            } else if (counter == s.get("maxCardRow") * s.get("maxCardColumn")) {
                xPosCard = s.get("initialXPosCard");
                yPosCard = s.get("initialYPosCard");
                counter = 0;
            }
        }
    }

    public static ArrayList<UICardImage> amountFilter(int page, ArrayList<UICardImage> uiCardImages){
        ArrayList<UICardImage> manager = new ArrayList<>();
        if (uiCardImages.size() <= 24)
            return uiCardImages;
        else if (uiCardImages.size() <= 48){
            if (page == 1) {
                for (int i = 0; i < 24; i++) {
                    manager.add(uiCardImages.get(i));
                }
            }else if (page == 2){
                for (int i = 24; i < uiCardImages.size(); i++) {
                    manager.add(uiCardImages.get(i));
                }
            }
            return manager;
        }else return null;
    }

    public static boolean canSell(Deck[] decks, Card card){
        for (Deck deck : decks) {
            if (deck != null){
                for (Card deckCard : deck.getCards()) {
                    if (deckCard.equals(card))
                        return false;
                }
            }
        }
        return true;
    }
}

