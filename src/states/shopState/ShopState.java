package states.shopState;

import Entity.cards.Card;
import gfx.Asserts;
import gfx.Drawer;
import gfx.ImageLoader;
import main.Handler;
import main.Util;
import otherModels.MainPlayerModel;
import otherModels.OtherModels;
import otherModels.configModel.ShopStateModel;
import states.Errors;
import states.State;
import ui.UICardImage;
import ui.UIManager;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class ShopState extends State {
    private ShopStateModel s = new ShopStateModel();
    private UIManager uiManager;
    private ArrayList<UICardImage> containCards;
    private ConstantButton constantButton;
    private boolean showUnLockCards = false;
    private boolean showLockCards = true;
    private int page = 1;
    private static String errorMassage = null;
    private static boolean error = false;

    public ShopState(Handler handler) throws IOException {
        super(handler);
        uiManager = new UIManager(handler);
        containCards = new ArrayList<>();
        constantButton = new ConstantButton(handler);
        cardInitFirst();
        setPicture();
        cardInit();
    }

    @Override
    public void tick() {
        uiManager.tick();
        constantButton.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.shopBackground, 0, 0, 1550, 878, null);
        uiManager.render(g2D);
        constantButton.render(g2D);
        if (errorMassage != null) {
            Errors.ErrorMassageShop(g2D, errorMassage);
        }
        Drawer.Wallet("Wallet: " + MainPlayerModel.getInstance().getDiamond(), g2D);
    }

    public void updateSetPicture() {
        try {
            setPicture();
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public void cardInitFirst() {
        for (Card card : MainPlayerModel.getInstance().getEntireCards()) {
            containCards.add(new UICardImage(card, 0, 0, s.get("widthCard"), s.get("heightCard"),
                    null, () -> {
                if (card.isLock()) {
                    if (MainPlayerModel.getInstance().getDiamond() >= card.getCost()) {
                        OtherModels.body("click card in shop", "buy " + card.getName());
                        MainPlayerModel.getInstance().getEntireCards().get(Util.searchMainPlayerCards(card)).setLock(false);
                        MainPlayerModel.getInstance().setDiamond(MainPlayerModel.getInstance().getDiamond() - card.getCost());
                        containCards.get(Util.searchMainPlayerCards(card)).getCard().setLock(false);
                        updateSetPicture();
                        cardInit();
                    } else {
                        error = true;
                        errorMassage = "Not enough money!";
                        OtherModels.body("error in shop state", "Not enough money!");
                    }
                } else {
                    if (CardInitUtils.canSell(MainPlayerModel.getInstance().getDecks(), card)) {
                        OtherModels.body("click card in shop state", "sell " + card.getName());
                        MainPlayerModel.getInstance().getEntireCards().get(Util.searchMainPlayerCards(card)).setLock(true);
                        MainPlayerModel.getInstance().setDiamond(MainPlayerModel.getInstance().getDiamond() + card.getCost());
                        containCards.get(Util.searchMainPlayerCards(card)).getCard().setLock(true);
                        updateSetPicture();
                        cardInit();
                    } else {
                        error = true;
                        errorMassage = "This card exists in a deck!";
                        OtherModels.body("error in shop state", "This card exists in a deck!");
                    }
                }
            }));
        }
    }

    public void cardInit() {
        ArrayList<UICardImage> manager;
        manager = states.collectionState.CardInitUtils.lockFilter(showUnLockCards, showLockCards, containCards);
        CardInitUtils.arrangeCards(manager);
        manager = CardInitUtils.amountFilter(page, manager);
        uiManager.setButtons(states.collectionState.CardInitUtils.toUIObject(manager));
    }

    public void setPicture() throws IOException {
        for (UICardImage uiCardImage : containCards) {
            if (uiCardImage.getCard().isLock())
                uiCardImage.setImages(ImageLoader.loadDoubleImage(uiCardImage.getCard().getTexturePath()[7], uiCardImage.getCard().getTexturePath()[10]));
            else
                uiCardImage.setImages(ImageLoader.loadDoubleImage(uiCardImage.getCard().getTexturePath()[0], uiCardImage.getCard().getTexturePath()[10]));
        }
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setShowUnLockCards(boolean showUnLockCards) {
        this.showUnLockCards = showUnLockCards;
    }

    public void setShowLockCards(boolean showLockCards) {
        this.showLockCards = showLockCards;
    }

    public static boolean isError() {
        return error;
    }

    public static void setErrorMassage(String errorMassage) {
        ShopState.errorMassage = errorMassage;
    }

    public static void setError(boolean error) {
        ShopState.error = error;
    }
}
