package states.infoPassive;

import otherModels.configModel.PassiveStateModel;
import ui.UIObject;
import ui.UIRecImage;

import java.util.ArrayList;

public class Utils {

    private static PassiveStateModel p = new PassiveStateModel();
    public static boolean infoNumber;

    public static ArrayList<UIObject> toUIObject(ArrayList<UIRecImage> uiRecImages) {
        ArrayList<UIObject> uiObjects = new ArrayList<>(uiRecImages);
        return uiObjects;
    }

    public static void arrange(ArrayList<UIRecImage> uiRecImages){
        int x = p.get("initialXPosCard");
        int y = p.get("initialYPosCard");
        for (UIRecImage uiRecImage : uiRecImages) {
            uiRecImage.setX(x);
            uiRecImage.setY(y);
            x += p.get("widthCard") + p.get("horizontalDistanceCard");
        }
    }

    public static boolean getInfoNumber() {
        return infoNumber;
    }

    public static void setInfoNumber(boolean infoNumber) {
        Utils.infoNumber = infoNumber;
    }
}
