package states.infoPassive;

import main.MainClient;

import java.io.IOException;

public class InfoPassiveModel {

    public static void transfer(boolean x1, int x2, int x3, int x4, boolean x5) {
        try {
            MainClient.getDataOutputStream().writeUTF("passive_" + x1 + "_" + x2 + "_" + x3 + "_" + x4 + "_" + x5);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
