package states.menuState;

import Entity.Deck;
import main.Handler;
import constants.Constants;
import gfx.Asserts;
import main.MainClient;
import main.Util;
import otherModels.MainPlayerModel;
import otherModels.OtherModels;
import otherModels.configModel.MenuStateModel;
import states.State;
import states.infoPassive.InfoPassive;
import states.playState.PlayState;
import states.rankState.RankState;
import states.statusState.StatusState;
import ui.UIRecImage;
import ui.UIManager;
import java.awt.*;
import java.io.IOException;

public class MenuState extends State {
    private MenuStateModel m = new MenuStateModel();
    private UIManager uiManager;
    private PlayState playState;
    private InfoPassive infoPassive;
    private StatusState statusState;
    private RankState rankState;

    public MenuState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);
        playInit();
        shopInit();
        statusInit();
        rankInit();
        collectionInit();
        Util.exit(uiManager);
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.menuBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        uiManager.render(g2D);
    }

    private void playInit() {
        uiManager.addButton(new UIRecImage(m.get("initialXPos"), m.get("initialYPos")
                , m.get("width"), m.get("height")
                , Asserts.play, () -> {
            OtherModels.body("click play", "click for playing in menu state");
            Deck deck = MainPlayerModel.getInstance().getDeck(0);
            if (deck == null)
                State.setCurrentState(handler.getCollectionState());
            else {
                OtherModels.body("hi","");
                handler.getCollectionState().getConstantButton().setTraining(false);
                String s = "";
                try {
                    MainClient.getDataOutputStream().writeUTF("startPlay_usual");
                    s = MainClient.getDataInputStream().readUTF();
                } catch (
                        IOException e) {
                    e.printStackTrace();
                }
                if (s.equals("play started")) {
                    infoPassive = new InfoPassive(handler);
                    handler.setInfoPassive1(infoPassive);
                    State.setCurrentState(infoPassive);
                }
            }
        }));
    }



    private void collectionInit() {
        uiManager.addButton(new UIRecImage(m.get("initialXPos"), m.get("initialYPos") +
                m.get("height") + m.get("verticalDistance"),
                m.get("width"), m.get("height"), Asserts.myCollectionIcon, () -> {
            State.setCurrentState(handler.getCollectionState());
            OtherModels.body("click myCollection button", "click for going to collection state");
        }));
    }

    private void shopInit() {
        uiManager.addButton(new UIRecImage(m.get("initialXPos"), m.get("initialYPos") +
                2 * m.get("height") + 2 * m.get("verticalDistance"),
                m.get("width"), m.get("height"), Asserts.shop, () -> {
            State.setCurrentState(handler.getShopState());
            OtherModels.body("click shop button", "click for going to shop state");
        }));
    }

    private void statusInit() {
        uiManager.addButton(new UIRecImage(m.get("initialXPos"), m.get("initialYPos") +
                3 * m.get("height") + 3 * m.get("verticalDistance"),
                m.get("width"), m.get("height"), Asserts.status, () -> {
            OtherModels.body("click status button", "click for going to status state");
            statusState = new StatusState(handler);
            handler.setStatusState(statusState);
            State.setCurrentState(statusState);
        }));
    }

    private void rankInit() {
        uiManager.addButton(new UIRecImage(m.get("initialXPos"), m.get("initialYPos") +
                4 * m.get("height") + 4 * m.get("verticalDistance"),
                m.get("width"), m.get("height"), Asserts.rank, () -> {
            OtherModels.body("click status button", "click for going to rank state");
            rankState = new RankState(handler);
            handler.setRankState(rankState);
            State.setCurrentState(rankState);
        }));
    }
}
