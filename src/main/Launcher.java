package main;


import login.LoginFrame;

public class Launcher {
    private static Game game;
    public static void main(String[] args) {
        new LoginFrame();
    }

    public static Game getGame() {
        return game;
    }

    public static void setGame(Game game) {
        Launcher.game = game;
    }
}

