package main;

import Entity.cards.Card;
import Entity.hero.Hero;
import Entity.hero.HeroClass;
import gfx.Asserts;
import otherModels.MainPlayerModel;
import otherModels.OtherModels;
import otherModels.configModel.MenuStateModel;
import ui.UIManager;
import ui.UIRecImage;

import java.io.IOException;

public class Util {
    private static MenuStateModel m = new MenuStateModel();

    public static int searchMainPlayerCards(Card card) {
        for (int i = 0; i < MainPlayerModel.getInstance().getEntireCards().size(); i++) {
            Card entireCard = MainPlayerModel.getInstance().getEntireCards().get(i);
            if (entireCard.equals(card))
                return i;
        }
        return -1;
    }

    public static void exit(UIManager uiManager) {
        uiManager.addButton(new UIRecImage(m.get("initialXPosMenu") + m.get("widthMenu") +
                m.get("horizontalDistance"), m.get("initialYPosMenu"), m.get("widthMenu"),
                m.get("heightMenu"), Asserts.exit, () -> {
            OtherModels.body("exit", "exit");
            OtherModels.saveChanges();
            try {
                MainClient.getSocket().close();
                MainClient.getSocket1().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.exit(0);
        }));
    }

    public static Hero finedHero(HeroClass heroClass){
        for (Hero hero : MainPlayerModel.getInstance().getHeroes()) {
            if (hero.getHeroClass().toString().equals(heroClass.toString())){
                return hero;
            }
        }
        return null;
    }

    public static Card findCardFromMainPlayer(String card){
        for (Card entireCard : MainPlayerModel.getInstance().getEntireCards()) {
            if (entireCard.getName().equals(card))
                return entireCard;
        }
        return null;
    }
}
