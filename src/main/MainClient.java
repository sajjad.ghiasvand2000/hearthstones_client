package main;

import login.LoginFrame;

import java.io.*;
import java.net.Socket;

public class MainClient {

    private static MainClient mainClient;
    private Socket socket;
    private Socket socket1;
    private DataInputStream dataInputStream;
    private DataInputStream dataInputStream1;
    private DataOutputStream dataOutputStream;
    private DataOutputStream dataOutputStream1;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    private ObjectInputStream objectInputStream1;

    private MainClient(String serverIP, int serverPort) {
        try {
            socket = new Socket(serverIP, serverPort);
            socket1 = new Socket(serverIP, serverPort);
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream1 = new DataOutputStream(socket1.getOutputStream());
            objectInputStream = new ObjectInputStream(socket.getInputStream());
            objectInputStream1 = new ObjectInputStream(socket1.getInputStream());
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataInputStream1 = new DataInputStream(socket1.getInputStream());
        } catch (IOException e) {
            LoginFrame.getTextArea().append("No server was found.\n");
        }
    }

    public static MainClient getInstance(String serverIP, int serverPort) {
        if (mainClient == null) {
            mainClient = new MainClient(serverIP, serverPort);
            if (mainClient.getSocket() != null) {
                if (mainClient.getSocket().isConnected()) {
                    LoginFrame.getTextArea().append("You connected to server successfully.\n");
                }
            } else mainClient = null;
            return mainClient;
        } else {
            return getInstance();
        }
    }

    public static MainClient getInstance() {
        return mainClient;
    }

    public static Socket getSocket() {
        return mainClient.socket;
    }

    public static Socket getSocket1() {
        return mainClient.socket1;
    }

    public static DataOutputStream getDataOutputStream(){
        return mainClient.dataOutputStream;
    }

    public static DataOutputStream getDataOutputStream1() {
        return mainClient.dataOutputStream1;
    }

    public static ObjectOutputStream getObjectOutputStream(){
        return mainClient.objectOutputStream;
    }

    public static DataInputStream getDataInputStream(){
        return mainClient.dataInputStream;
    }

    public static DataInputStream getDataInputStream1(){
        return mainClient.dataInputStream1;
    }
    public static ObjectInputStream getObjectInputStream(){
        return mainClient.objectInputStream;
    }
    public static ObjectInputStream getObjectInputStream1(){
        return mainClient.objectInputStream1;
    }

    public static void setMainClient(MainClient mainClient) {
        MainClient.mainClient = mainClient;
    }
}
