package Entity;

import Entity.cards.Card;
import Entity.cards.Type;
import Entity.cards.minion.Minion;
import Entity.cards.spell.Spell;
import Entity.cards.weapon.Weapon;
import Entity.hero.Hero;
import Entity.viewModel.*;
import java.util.ArrayList;
import java.util.List;

public class EntityViewModel {

    public Deck getDeck(DeckViewModel deckViewModel) {
        if (deckViewModel == null)
            return null;
        else {
            List<Card> cards = new ArrayList<>();
            for (MinionViewModel minionViewModel : deckViewModel.getMinionViewModels())
                cards.add(getMinion(minionViewModel));
            for (SpellViewModel spellViewModel : deckViewModel.getSpellViewModels())
                cards.add(getSpell(spellViewModel));
            for (WeaponViewModel weaponViewModel : deckViewModel.getWeaponViewModels())
                cards.add(getWeapon(weaponViewModel));
            return new Deck(cards, getHero(deckViewModel.getHero()), deckViewModel.getTexturePath(), deckViewModel.getName(),
                    deckViewModel.getNumberGame(), deckViewModel.getNumberWin());
        }
    }

    public Minion getMinion(MinionViewModel minionViewModel) {
        if (minionViewModel == null)
            return null;
        else {
            return new Minion(minionViewModel.getName(), minionViewModel.getCost(), minionViewModel.getMana(), minionViewModel.getRarity(), minionViewModel.getHeroClass(),
                    minionViewModel.getType(), minionViewModel.getDescription(), minionViewModel.getHP(), minionViewModel.getAttack(), minionViewModel.getTexturePath(), minionViewModel.isLock(), minionViewModel.getNumberDraw());
        }
    }

    public Spell getSpell(SpellViewModel spellViewModel) {
        if (spellViewModel == null) {
            return null;
        } else {
            return new Spell(spellViewModel.getName(), spellViewModel.getCost(), spellViewModel.getMana(), spellViewModel.getRarity(),
                    spellViewModel.getHeroClass(), spellViewModel.getType(), spellViewModel.getDescription(), "", spellViewModel.getTexturePath(),
                    spellViewModel.isLock(), spellViewModel.getNumberDraw());
        }
    }

    public Weapon getWeapon(WeaponViewModel weaponViewModel) {
        if (weaponViewModel == null) {
            return null;
        } else {
            return new Weapon(weaponViewModel.getName(), weaponViewModel.getCost(), weaponViewModel.getMana(), weaponViewModel.getRarity(),
                    weaponViewModel.getHeroClass(), weaponViewModel.getType(), weaponViewModel.getDescription(), weaponViewModel.getDurability(),
                    weaponViewModel.getAttack(), weaponViewModel.getTexturePath(), weaponViewModel.isLock(), weaponViewModel.getNumberDraw());
        }
    }

    public Hero getHero(HeroViewModel heroViewModel) {
        if (heroViewModel == null)
            return null;
        else {
            return new Hero(heroViewModel.getHeroClass(), heroViewModel.getHP(), heroViewModel.getMAX_HP(), heroViewModel.getTexturePath(), heroViewModel.getHeroTexture());
        }
    }

    public Card getCard(CardViewModel cardViewModel){
        if (cardViewModel == null)
            return null;
        else {
            return new Card(cardViewModel.getName(), cardViewModel.getCost(), cardViewModel.getMana(), cardViewModel.getRarity(),
                    cardViewModel.getHeroClass(), cardViewModel.getType(), cardViewModel.getDescription(), cardViewModel.getTexturePath(),
                    cardViewModel.isLock(), cardViewModel.getNumberDraw());
        }
    }

    public ArrayList<Card> getCards(ArrayList<CardViewModel> cardViewModels){
        if (cardViewModels == null){
            return null;
        }else {
            ArrayList<Card> cards = new ArrayList<>();
            for (CardViewModel cardViewModel : cardViewModels) {
                cards.add(getCard(cardViewModel));
            }
            return cards;
        }
    }

    public ArrayList<Minion> getMinions(ArrayList<MinionViewModel> minionViewModels){
        if (minionViewModels == null){
            return null;
        }else {
            ArrayList<Minion> cards = new ArrayList<>();
            for (MinionViewModel cardViewModel : minionViewModels) {
                cards.add(getMinion(cardViewModel));
            }
            return cards;
        }
    }

    public ArrayList<Spell> getSpells(ArrayList<SpellViewModel> spellViewModels){
        if (spellViewModels == null){
            return null;
        }else {
            ArrayList<Spell> cards = new ArrayList<>();
            for (SpellViewModel spellViewModel : spellViewModels) {
                cards.add(getSpell(spellViewModel));
            }
            return cards;
        }
    }

    public ArrayList<Weapon> getWeapons(ArrayList<WeaponViewModel> weaponViewModels){
        if (weaponViewModels == null){
            return null;
        }else {
            ArrayList<Weapon> weapons = new ArrayList<>();
            for (WeaponViewModel weaponViewModel : weaponViewModels) {
               weapons.add(getWeapon(weaponViewModel));
            }
            return weapons;
        }
    }

    public List<Hero> getHeroes(List<HeroViewModel> heroViewModels){
        if (heroViewModels == null){
            return null;
        }else {
            List<Hero> heroes = new ArrayList<>();
            for (HeroViewModel heroViewModel : heroViewModels) {
                heroes.add(getHero(heroViewModel));
            }
            return heroes;
        }
    }

    public Deck[] getDecks(DeckViewModel[] deckViewModels){
        if (deckViewModels == null)
            return null;
        else {
            Deck[] decks = new Deck[deckViewModels.length];
            for (int i = 0 ; i < decks.length ; i++){
                decks[i] = getDeck(deckViewModels[i]);
            }
            return decks;
        }
    }

    public Minion[] getArrayMinions(MinionViewModel[] minionViewModels){
        if (minionViewModels == null)
            return null;
        else {
            Minion[] minions = new Minion[minionViewModels.length];
            for (int i = 0 ; i < minions.length ; i++){
                minions[i] = getMinion(minionViewModels[i]);
            }
            return minions;
        }
    }

    public DeckViewModel setDeck(Deck deck){
        if (deck == null)
            return null;
        else {
            ArrayList<SpellViewModel> spellViewModels = new ArrayList<>();
            ArrayList<WeaponViewModel> weaponViewModels = new ArrayList<>();
            ArrayList<MinionViewModel> minionViewModels = new ArrayList<>();
            for (Card card : deck.getCards()) {
                if (card.getType().equals(Type.MINION))
                    minionViewModels.add(setMinion((Minion) card));
                else if (card.getType().equals(Type.SPELL))
                    spellViewModels.add(setSpell((Spell) card));
                else weaponViewModels.add(setWeapon((Weapon)card));
            }
            return new DeckViewModel(spellViewModels, weaponViewModels, minionViewModels, setHero(deck.getHero()), deck.getTexturePath(), deck.getName(),
                    deck.getNumberGame(), deck.getNumberWin());
        }
    }

    public CardViewModel setCard(Card card){
        if (card == null)
            return null;
        else {
            return new CardViewModel(card.getName(), card.getCost(), card.getMana(), card.getRarity(), card.getHeroClass(),
                    card.getType(), card.getDescription(), card.getTexturePath(), card.isLock(), card.getNumberDraw());
        }
    }


    public HeroViewModel setHero(Hero hero){
        if (hero == null)
            return null;
        else {
            return new HeroViewModel(hero.getHeroClass(), hero.getHP(), hero.getMAX_HP(), hero.getTexturePath(), hero.getHeroTexture());
        }
    }

    public SpellViewModel setSpell(Spell spell){
        if (spell == null)
            return null;
        else {
            return new SpellViewModel(spell.getName(), spell.getCost(), spell.getMana(), spell.getRarity(), spell.getHeroClass(),
                    spell.getType(), spell.getDescription(), spell.getTexturePath(), spell.isLock(), spell.getNumberDraw());
        }
    }

    public MinionViewModel setMinion(Minion minion){
        if (minion == null)
            return null;
        else {
            return new MinionViewModel(minion.getName(), minion.getCost(), minion.getMana(), minion.getRarity(), minion.getHeroClass(),
                    minion.getType(), minion.getDescription(), minion.getTexturePath(), minion.isLock(), minion.getNumberDraw(), minion.getHP(),
                    minion.getAttack(), minion.getMinionTexture());
        }
    }
    public WeaponViewModel setWeapon(Weapon card){
        if (card == null)
            return null;
        else {
            return new WeaponViewModel(card.getName(), card.getCost(), card.getMana(), card.getRarity(), card.getHeroClass(),
                    card.getType(), card.getDescription(), card.getTexturePath(), card.isLock(), card.getNumberDraw(), card.getDurability(),
                    card.getAttack(), card.getWeaponTexture());
        }
    }
}
