package Entity;

import java.util.ArrayList;

public class EntityUtils {

    public static String buildName(String s) {
        String built = "";
        String[] names = s.split(" ");
        for (String name : names) {
            built += name;
        }
        return built;
    }

    public static String[] convertArrayListToArray(ArrayList arrayList) {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            strings.add((String) arrayList.get(i));
        }
        String[] strings1 = new String[arrayList.size()];
        return strings.toArray(strings1);
    }
}
