package Entity.viewModel;

import Entity.cards.weapon.Weapon;

import java.io.Serializable;
import java.util.List;

public class DeckViewModel implements Serializable {

    private List<SpellViewModel> spellViewModels;
    private List<WeaponViewModel> weaponViewModels;
    private List<MinionViewModel> minionViewModels;
    private HeroViewModel hero;
    private String texturePath;
    private String name;
    private int numberGame;
    private int numberWin;

    public DeckViewModel(List<SpellViewModel> spellViewModels, List<WeaponViewModel> weaponViewModels, List<MinionViewModel> minionViewModels, HeroViewModel hero, String texturePath, String name, int numberGame, int numberWin) {
        this.spellViewModels = spellViewModels;
        this.weaponViewModels = weaponViewModels;
        this.minionViewModels = minionViewModels;
        this.hero = hero;
        this.texturePath = texturePath;
        this.name = name;
        this.numberGame = numberGame;
        this.numberWin = numberWin;
    }

    public List<SpellViewModel> getSpellViewModels() {
        return spellViewModels;
    }

    public void setSpellViewModels(List<SpellViewModel> spellViewModels) {
        this.spellViewModels = spellViewModels;
    }

    public List<WeaponViewModel> getWeaponViewModels() {
        return weaponViewModels;
    }

    public void setWeaponViewModels(List<WeaponViewModel> weaponViewModels) {
        this.weaponViewModels = weaponViewModels;
    }

    public List<MinionViewModel> getMinionViewModels() {
        return minionViewModels;
    }

    public void setMinionViewModels(List<MinionViewModel> minionViewModels) {
        this.minionViewModels = minionViewModels;
    }

    public HeroViewModel getHero() {
        return hero;
    }

    public void setHero(HeroViewModel hero) {
        this.hero = hero;
    }

    public String getTexturePath() {
        return texturePath;
    }

    public void setTexturePath(String texturePath) {
        this.texturePath = texturePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberGame() {
        return numberGame;
    }

    public void setNumberGame(int numberGame) {
        this.numberGame = numberGame;
    }

    public int getNumberWin() {
        return numberWin;
    }

    public void setNumberWin(int numberWin) {
        this.numberWin = numberWin;
    }
}
