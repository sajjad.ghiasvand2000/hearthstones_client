package Entity.hero;

import Entity.cards.Card;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public  class Hero{

    private HeroClass heroClass;
    protected int HP;
    protected int MAX_HP;
    private List<Card> cards;
    private String[] texturePath;
    private String[] heroTexture;

    public Hero(HeroClass heroClass, int HP) {
        this.heroClass = heroClass;
        this.HP = HP;
    }

    public Hero(HeroClass heroClass, String[] texturePath, String[] heroTexture) {
        this.heroClass = heroClass;
        this.texturePath = texturePath;
        this.heroTexture = heroTexture;
    }

    public Hero(HeroClass heroClass, int HP, int MAX_HP, String[] texturePath, String[] heroTexture) {
        this.heroClass = heroClass;
        this.HP = HP;
        this.MAX_HP = MAX_HP;
        this.texturePath = texturePath;
        this.heroTexture = heroTexture;
    }

    public Hero(Hero hero){
        this.heroClass = hero.heroClass;
        this.cards = new ArrayList<>(hero.cards);
        this.texturePath = hero.texturePath;
    }

    public Hero() {}

    public Hero(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cardsName) {
        this.cards = cardsName;
    }

    public HeroClass getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public int getHP() {
        return HP;
    }

    public String[] getHeroTexture() {
        return heroTexture;
    }

    public void setHeroTexture(String[] heroTexture) {
        this.heroTexture = heroTexture;
    }

    public void setTexturePath(String[] texturePath) {
        this.texturePath = texturePath;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getMAX_HP() {
        return MAX_HP;
    }

    public void setMAX_HP(int MAX_HP) {
        this.MAX_HP = MAX_HP;
    }

    public String[] getTexturePath() {
        return texturePath;
    }
}
