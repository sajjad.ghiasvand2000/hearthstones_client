package Entity.hero;

public class Mage extends Hero {

    public Mage() {
    }

    public Mage(Hero hero){
        super(hero);
        HP = 30;
        MAX_HP = 30;
    }

}
