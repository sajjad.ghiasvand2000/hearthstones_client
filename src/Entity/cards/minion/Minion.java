package Entity.cards.minion;

import Entity.EntityUtils;
import Entity.cards.Card;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import com.google.gson.internal.LinkedTreeMap;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class Minion extends Card {
    protected Integer HP;
    private Integer attack;
    protected boolean charge;
    private boolean rush;
    private boolean shield;
    private boolean taunt;
    private boolean poisonous;
    private boolean windFury;
    private boolean lifeSteal;
    private int numberWinFury;
    private boolean update;
    private boolean doubleDamage;
    private String[] MinionTexture;

    public Minion(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock, int numberDraw) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock, numberDraw);
        MinionTexture = new String[2];
        this.HP = HP;
        this.attack = attack;
    }

    public Minion(Minion minion){
        super(minion);
        this.attack = minion.attack;
        this.HP = minion.HP;
        this.charge = minion.charge;
        this.MinionTexture = minion.MinionTexture;
    }

    public static Object factory(LinkedTreeMap map) {
        Class clazz = null;
        try {
            clazz = Class.forName("Entity.cards.minion." + EntityUtils.buildName((String) map.get("name")));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor constructor = clazz.getConstructors()[0];
        Double cost = (Double) map.get("cost");
        Double mana = (Double) map.get("mana");
        Double HP = (Double) map.get("HP");
        Double attack = (Double) map.get("attack");
        Object o = null;
        try {
            o = constructor.newInstance(map.get("name"), cost.intValue(), mana.intValue(), Rarity.valueOf((String)map.get("rarity")),
                    HeroClass.valueOf((String)map.get("heroClass")), Type.valueOf((String)map.get("type")), map.get("description"),
                    HP.intValue(), attack.intValue(), EntityUtils.convertArrayListToArray((ArrayList)map.get("texturePath")), map.get("lock"));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return o;
    }

    public Minion factory2() {
        Class clazz = null;
        try {
            clazz = Class.forName("Entity.cards.minion." + EntityUtils.buildName(name));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor constructor = clazz.getConstructors()[0];
        Object o = null;
        try {
            o = constructor.newInstance(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (Minion) o;
    }

    public void setCharge(boolean charge) {
        this.charge = charge;
    }

    public Integer getHP() {
        return HP;
    }

    public void setHP(Integer HP) {
        this.HP = HP;
    }

    public Integer getAttack() {
        return attack;
    }

    public String[] getMinionTexture() {
        return MinionTexture;
    }

    public void setMinionTexture(String[] minionTexture) {
        MinionTexture = minionTexture;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public boolean isRush() {
        return rush;
    }

    public void setRush(boolean rush) {
        this.rush = rush;
    }

    public void setShield(boolean shield) {
        this.shield = shield;
    }

    public boolean isShield() {
        return shield;
    }

    public boolean isTaunt() {
        return taunt;
    }

    public void setTaunt(boolean taunt) {
        this.taunt = taunt;
    }

    public boolean isWindFury() {
        return windFury;
    }

    public void setWindFury(boolean windFury) {
        this.windFury = windFury;
    }

    public int getNumberWinFury() {
        return numberWinFury;
    }

    public void setNumberWinFury(int numberWinFury) {
        this.numberWinFury = numberWinFury;
    }

    public boolean isPoisonous() {
        return poisonous;
    }

    public void setPoisonous(boolean poisonous) {
        this.poisonous = poisonous;
    }

    public boolean isLifeSteal() {
        return lifeSteal;
    }

    public void setLifeSteal(boolean lifeSteal) {
        this.lifeSteal = lifeSteal;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public boolean isDoubleDamage() {
        return doubleDamage;
    }

    public void setDoubleDamage(boolean doubleDamage) {
        this.doubleDamage = doubleDamage;
    }
}
