package otherModels;

import Entity.Deck;
import Entity.EntityViewModel;
import Entity.viewModel.DeckViewModel;
import main.MainClient;

import java.io.IOException;

public class OtherModels {
    private static EntityViewModel entityViewModel = new EntityViewModel();

    public synchronized static void body(String request1, String request2){
        try {
            MainClient.getDataOutputStream().writeUTF("log_" + request1 + "_" + request2);
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void saveChanges(){
        try {
            MainClient.getDataOutputStream().writeUTF("save_ ");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void setDeck(Deck deck){
        try {
            MainClient.getDataOutputStream().writeUTF("collection_" + "deck");
            MainClient.getDataOutputStream().flush();
            MainClient.getObjectOutputStream().writeObject(entityViewModel.setDeck(deck));
            MainClient.getObjectOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static Deck getDeck(){
        try {
            MainClient.getDataOutputStream().writeUTF("changeCard_" + "deck");
            MainClient.getDataOutputStream().flush();
            return entityViewModel.getDeck((DeckViewModel) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized static void changeGamePlayer(){
        try {
            MainClient.getDataOutputStream().writeUTF("changeGamePlayer");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void endTurnClicked(){
        try {
            MainClient.getDataOutputStream().writeUTF("endTurnClicked");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void summonMinion(int i){
        try {
            MainClient.getDataOutputStream().writeUTF("summonMinion_" + i);
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void massage(String massage){
        try {
            MainClient.getDataOutputStream().writeUTF("massage_" + massage);
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void statusState(){
        try {
            MainClient.getDataOutputStream().writeUTF("statusState");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void rankState(){
        try {
            MainClient.getDataOutputStream().writeUTF("rankState");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static void exitInPlay(){
        try {
            MainClient.getDataOutputStream().writeUTF("exitInPlay");
            MainClient.getDataOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
