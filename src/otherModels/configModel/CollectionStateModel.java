package otherModels.configModel;

import main.MainClient;

import java.io.IOException;

public class CollectionStateModel {

    public int get(String s){
        try {
            MainClient.getDataOutputStream().writeUTF("config_collection_" + s);
            return Integer.parseInt(MainClient.getDataInputStream().readUTF());
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
