package otherModels;

import Entity.Deck;
import Entity.EntityViewModel;
import Entity.cards.Card;
import Entity.cards.minion.Minion;
import Entity.cards.spell.Spell;
import Entity.cards.weapon.Weapon;
import Entity.hero.Hero;
import Entity.viewModel.*;
import main.MainClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainPlayerModel {

    private static MainPlayerModel mainPlayerModel = new MainPlayerModel();
    private EntityViewModel entityViewModel;
    private MainPlayerModel(){
        entityViewModel = new EntityViewModel();
    }

    public static MainPlayerModel getInstance(){
        return mainPlayerModel;
    }

    public Deck getDeck(int i){
        try {
            MainClient.getDataOutputStream().writeUTF("MainPlayer_" + "deck_" + i);
            return entityViewModel.getDeck((DeckViewModel) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setDeck(Deck deck, int i){
        try {
            MainClient.getDataOutputStream().writeUTF("MainPlayer_" + "setDeck_" + i);
            MainClient.getObjectOutputStream().writeObject(entityViewModel.setDeck(deck));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Card> getEntireCards(){
        try {
            MainClient.getDataOutputStream().writeUTF("MainPlayer_" + "entireCards");
            ArrayList<Card> cards = new ArrayList<>();
            ArrayList<Minion> minions = entityViewModel.getMinions((ArrayList<MinionViewModel>) MainClient.getObjectInputStream().readObject());
            ArrayList<Spell> spells = entityViewModel.getSpells((ArrayList<SpellViewModel>) MainClient.getObjectInputStream().readObject());
            ArrayList<Weapon> weapons = entityViewModel.getWeapons((ArrayList<WeaponViewModel>) MainClient.getObjectInputStream().readObject());
            cards.addAll(minions);
            cards.addAll(spells);
            cards.addAll(weapons);
            return cards;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getUserId(){
        try {
            MainClient.getDataOutputStream().writeUTF("MainPlayer_" + "userId");
            return  Integer.parseInt(MainClient.getDataInputStream().readUTF());
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getDiamond(){
        try {
            MainClient.getDataOutputStream().writeUTF("MainPlayer_" + "diamond");
            return  Integer.parseInt(MainClient.getDataInputStream().readUTF());
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void setDiamond(int i){
        try {
            MainClient.getDataOutputStream().writeUTF("MainPlayer_" + "setDiamond_" + i);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Hero> getHeroes(){
        try {
            MainClient.getDataOutputStream().writeUTF("MainPlayer_" + "heroes");
            return entityViewModel.getHeroes((List<HeroViewModel>) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Deck[] getDecks(){
        try {
            MainClient.getDataOutputStream().writeUTF("MainPlayer_" + "decks");
            return entityViewModel.getDecks((DeckViewModel[]) MainClient.getObjectInputStream().readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
